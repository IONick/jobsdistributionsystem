
package model;


public class Request {
    
    private String requesterLogin;
    private String requesterName;
    private String job;
    private double payment;
    private double hoursInWeek;
    private String phoneNumber;
    private int type;  // 1 - запрос на поиск специалиста, 2 - запрос на поиск работодателя
    
    
    public Request(String requesterLogin, String job, double payment, double hoursInWeek,String phoneNumber, int type){
        this.requesterLogin = requesterLogin;
        this.job = job;
        this.payment = payment;
        this.hoursInWeek = hoursInWeek;
        this.phoneNumber = phoneNumber;
        this.type = type;
    }
    
    public String getRequesterLogin(){
        return requesterLogin;
    }
    
    public String getRequesterName(){
        return requesterName;
    }
    
    public String getJob(){
        return job;
    }
    
    public double getPayment(){
        return payment;
    }
    
    public double getHours(){
        return hoursInWeek;
    }
    
    public String getNumber(){
        return phoneNumber;
    }
    
    public int getType(){
        return type;
    }
    
    public void setRequesterName(String name){
        requesterName = name;
    }
        
}
