
package model;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

public class Client{
    
    private List <Request> incomingRequests;
    private String name;
    private String login;
    private String password;
    
    public Client(String name, String login, String password){
        this.name = name;
        this.login = login;
        this.password = password;
    }
    
     public String getName(){
        return name;
    }
    
    public String getLogin(){
        return login;
    }
    
    public String getPassword(){
        return password;
    }
    
    public void addRequest(Request request){
        if(incomingRequests == null){
            incomingRequests = new ArrayList<>();
        }
        incomingRequests.add(request);
    }
    
    public void printRequests(){
        if(incomingRequests == null){
            System.out.println("Входящих предложений нет");
            return;
        }
        Iterator<Request> iterator = incomingRequests.iterator();
        while(iterator.hasNext()){
            Request request = iterator.next();
            System.out.println("---------------------------------------------------------");
            System.out.println("");
            if(request.getType() == 1){
                if(request.getRequesterName() != null){
                    System.out.println("Название организации: " + request.getRequesterName());                    
                }
                System.out.println("Необходимая должность: " + request.getJob());
                System.out.println("Предлагаемая заработная плата: " + request.getPayment() + " $");
                
            }
            else if(request.getType() == 2){
                if(request.getRequesterName() != null){
                    System.out.println("ФИО специалиста: " + request.getRequesterName());                    
                }
                System.out.println("Предлагаемая должность: " + request.getJob());
                System.out.println("Желаемая заработная плата: " + request.getPayment() + " $");
            }
            System.out.println("Недельная занятость: " + request.getHours() + " часов");
            System.out.println("Контактный номер телефона: " + request.getNumber());
            System.out.println("");
            System.out.println("---------------------------------------------------------");
            System.out.println("");
        }
    }
    
    public void deleteAllRequests(){
        incomingRequests = null;
    }
    
    @Override
    public boolean equals(Object ob){
        if(ob == null) return false;
        if(!(ob instanceof Client)) return false;
        if(!((Client) ob).login.equals(this.login))return false;
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.login.hashCode();
        return hash;
    }
       
}
