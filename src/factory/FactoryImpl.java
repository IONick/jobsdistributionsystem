
package factory;

import dao.ClientDao;
import dao.RequestDao;
import dao.ClientDaoImpl;
import dao.RequestDaoImpl;

public class FactoryImpl extends Factory{
    
    @Override
    public ClientDao createClientDao(){
        return new ClientDaoImpl();
    }
    
    @Override
    public RequestDao createRequestDao(){
        return new RequestDaoImpl();
    }
}
