
package factory;

import dao.ClientDao;
import dao.RequestDao;

public abstract class Factory {
    public abstract ClientDao createClientDao();
    public abstract RequestDao createRequestDao();
}
