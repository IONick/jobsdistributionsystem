
package dao;

import java.util.List;
import model.Client;
import java.sql.SQLException;

public interface ClientDao {
    boolean isFound(String login, String password) throws SQLException;
    boolean isFound(String login) throws SQLException;
    Client getClient(String login) throws SQLException, NullPointerException;
    List<Client> getClientsList() throws SQLException;
    void addClient(Client client) throws SQLException;
}
