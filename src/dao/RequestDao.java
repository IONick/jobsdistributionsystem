
package dao;

import java.sql.SQLException;
import java.util.List;
import model.Request;
import model.Client;

public interface RequestDao {
    List<Request> getRequestsList() throws SQLException;
    void addRequest(Request request) throws SQLException;
    int deleteAllRequests(Client client) throws SQLException;
    int countEmployersRequests() throws SQLException;
    int countWorkersRequests() throws SQLException;
}
