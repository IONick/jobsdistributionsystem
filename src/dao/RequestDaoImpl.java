
package dao;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import model.Request;
import model.Client;
import java.util.List;
import java.util.ArrayList;

public class RequestDaoImpl implements RequestDao{
    
    private Connection connection;
    private Statement statement;
    private PreparedStatement prstatement;
    private ResultSet resSet;
    
    @Override
    public List<Request> getRequestsList() throws SQLException{
        ArrayList<Request> arr = new ArrayList<>();
        try{
            connection = connect();
            statement = connection.createStatement();
            String query = "SELECT * FROM requests;";
            resSet = statement.executeQuery(query);
            String login;
            String job;
            double payment;
            double hours;
            String number;
            int type;
            while(resSet.next()){
                login = resSet.getString("login");
                job = resSet.getString("job");
                payment = resSet.getDouble("payment");
                hours = resSet.getDouble("hours_in_week");
                number = resSet.getString("phone_number");
                type = resSet.getInt("type_of_request");
                arr.add(new Request(login, job, payment, hours, number, type));
            }
        }catch(SQLException e){
            System.out.println("Exception: " + e);
            e.printStackTrace();
        }
        finally{
            if(resSet != null) resSet.close();
            if(statement != null) statement.close();
            if(connection != null) connection.close();
        }
        return arr;
    }
    
    @Override
    public void addRequest(Request request) throws SQLException{
        try{
            connection = connect();
            String query = "INSERT INTO requests(login, job, payment, hours_in_week, phone_number, type_of_request) VALUES(?, ?, ?, ?, ?, ?)";
            prstatement = connection.prepareStatement(query);
            prstatement.setString(1, request.getRequesterLogin());
            prstatement.setString(2, request.getJob());
            prstatement.setDouble(3, request.getPayment());
            prstatement.setDouble(4, request.getHours());
            prstatement.setString(5, request.getNumber());
            prstatement.setInt(6, request.getType());
            prstatement.executeUpdate();
        }catch(SQLException e){
            System.out.println("Exception: " + e);
            e.printStackTrace();
        }
        finally{
            if(prstatement != null) prstatement.close();
            if(connection != null) connection.close();
        }
    }
    
    @Override
    public int deleteAllRequests(Client client) throws SQLException{
        int number = 0;
        try{
            connection = connect();
            String query = "DELETE FROM requests WHERE login = ?;";
            prstatement = connection.prepareStatement(query);
            prstatement.setString(1, client.getLogin());
            number = prstatement.executeUpdate();
        }catch(SQLException e){
            System.out.println("Exception: " + e);
            e.printStackTrace();
        }finally{
            if(prstatement != null) prstatement.close();
            if(connection != null) connection.close();
        }
        return number;
    }
    
    @Override
    public int countEmployersRequests() throws SQLException{
        return countClientsRequests(1);
    }
    
    @Override
    public int countWorkersRequests() throws SQLException{
        return countClientsRequests(2);
    }
    
    private int countClientsRequests(int request_type) throws SQLException{
        int count = 0;
        if(request_type != 1 && request_type != 2){
            System.out.println("Error of input request_type");
            return count;
        }
        try{
            connection = connect();
            String query = "SELECT COUNT(*) FROM requests WHERE type_of_request = ?;";
            prstatement = connection.prepareStatement(query);
            prstatement.setInt(1, request_type);
            resSet = prstatement.executeQuery();
            while(resSet.next()){
                count = resSet.getInt(1);
            }
        }catch(SQLException e){
            System.out.println("Exception: " + e);
            e.printStackTrace();
        }
        finally{
            if(resSet != null) resSet.close();
            if(prstatement != null) prstatement.close();
            if(connection != null) connection.close();
        }
        return count;
    }
    
    private Connection connect() throws SQLException{
        String url = "jdbc:mysql://localhost:3306/jobs_distribution_system?autoReconnect=true&useSSL=false";
        String user = "root";
        String password = "admin";
        Connection conn = DriverManager.getConnection(url, user, password);
        return conn;
    }
    
}
