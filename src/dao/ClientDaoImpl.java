
package dao;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import model.Client;
import java.util.List;
import java.util.ArrayList;


public class ClientDaoImpl implements ClientDao{
    
    private Connection connection;
    private Statement statement;
    private PreparedStatement prstatement;
    private ResultSet resSet;
    
    @Override
    public boolean isFound(String login, String password) throws SQLException{
        boolean isFound = false;
        try{
            connection = connect();
            String query = "SELECT * FROM users WHERE login = ? AND password = ?;";
            prstatement = connection.prepareStatement(query);
            prstatement.setString(1, login);
            prstatement.setString(2, password);
            resSet = prstatement.executeQuery();
            isFound = resSet.next();
        }catch(SQLException e){
            System.out.println("Exception: " + e);
            e.printStackTrace();
        }
        finally{
            if(resSet != null) resSet.close();
            if(prstatement != null) prstatement.close();
            if(connection != null) connection.close();
        }
        return isFound;
    }
    
    @Override
    public boolean isFound(String login) throws SQLException{
        boolean isFound = false;
        try{
            connection = connect();
            String query = "SELECT * FROM users WHERE login = ?;";
            prstatement = connection.prepareStatement(query);
            prstatement.setString(1, login);
            resSet = prstatement.executeQuery();
            isFound = resSet.next();
        }catch(SQLException e){
            System.out.println("Exception: " + e);
            e.printStackTrace();
        }
        finally{
            if(resSet != null) resSet.close();
            if(prstatement != null) prstatement.close();
            if(connection != null) connection.close();
        }
        return isFound;
    }
    
    @Override
    public Client getClient(String login) throws SQLException, NullPointerException{
        String password = null;
        String name = null;
        try{
            connection = connect();
            String query = "SELECT * FROM users WHERE login = ?;";
            prstatement = connection.prepareStatement(query);
            prstatement.setString(1, login);
            resSet = prstatement.executeQuery();
            while(resSet.next()){
                password = resSet.getString("password");
                name = resSet.getString("user_name");
            }
        }catch(SQLException e){
            System.out.println("Exception: " + e);
            e.printStackTrace();
        }
        finally{
            if(resSet != null) resSet.close();
            if(prstatement != null) prstatement.close();
            if(connection != null) connection.close();
        }
        if(password == null|| name == null){
            throw new NullPointerException();
        }
        Client client = new Client(name, login, password);
        return client;
    }
    
    @Override
    public List<Client> getClientsList() throws SQLException{
        ArrayList<Client> arr = new ArrayList<>();
        try{
            connection = connect();
            statement = connection.createStatement();
            String query = "SELECT * FROM users;";
            resSet = statement.executeQuery(query);
            String login;
            String password;
            String name;
            while(resSet.next()){
                login = resSet.getString("login");
                password = resSet.getString("password");
                name = resSet.getString("user_name");
                arr.add(new Client(name, login, password));
            }
        }catch(SQLException e){
            System.out.println("Exception: " + e);
            e.printStackTrace();
        }
        finally{
            if(resSet != null) resSet.close();
            if(statement != null) statement.close();
            if(connection != null) connection.close();
        }
        return arr;
    }
    
    @Override
    public void addClient(Client client) throws SQLException{
        if(client == null){
            System.out.println("Null pointer of client");
            return;
        }
        try{
            connection = connect();
            String query = "INSERT INTO users VALUES (?, ?, ?)";
            prstatement = connection.prepareStatement(query);
            prstatement.setString(1, client.getLogin());
            prstatement.setString(2, client.getPassword());
            prstatement.setString(3, client.getName());
            prstatement.executeUpdate();
        }catch(SQLException e){
            System.out.println("Exception: " + e);
            e.printStackTrace();
        }
        finally{
            if(prstatement != null) prstatement.close();
            if(connection != null) connection.close();
        }
    }
    
    private Connection connect() throws SQLException{
        String url = "jdbc:mysql://localhost:3306/jobs_distribution_system?autoReconnect=true&useSSL=false";
        String user = "root";
        String password = "admin";
        Connection conn = DriverManager.getConnection(url, user, password);
        return conn;
    }
    
}
