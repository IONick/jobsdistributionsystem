
package system;

import dao.ClientDao;
import dao.RequestDao;
import factory.Factory;
import factory.FactoryImpl;
import java.util.Scanner;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import model.Client;
import model.Request;


public class LabourSystem {
    
    private Scanner scan;
    private ClientDao clientDao;
    private RequestDao requestDao;
    private Client currentClient;
    private Set<Client> clientsSet;
    private List<Request> employerRequests;
    private List<Request> workerRequests;
    
    public LabourSystem(){
        scan = new Scanner(System.in, "Cp1251");
        Factory factory = new FactoryImpl();
        clientDao = factory.createClientDao();
        requestDao = factory.createRequestDao();
    }
            

   
    public static void main(String[] args) {
        LabourSystem system = new LabourSystem();
        system.menu();
    }
    
    
    
    public void menu(){
        boolean flag = true;
        while(flag){
            System.out.println("1) Вход в систему");
            System.out.println("2) Регистрация в системе");
            System.out.println("3) Выход");
            System.out.print("Ввод: ");
            String str = scan.nextLine();
            switch(str){
                case "1":{
                    entrance();
                    break;
                }
                case "2":{
                    registerClient();
                    break;
                }
                case "3":{
                    flag = false;
                    break;
                }
                default: System.out.println("Ошибка ввода");
            }
        }
    }
    
    private void entrance(){
        boolean flag = true;
        while(flag){
            System.out.println("Введите логин и пароль (Введите 'exit' для возврата в меню)");
            System.out.print("Логин: ");
            String login = scan.nextLine();
            if(login.equals("exit")) return;
            System.out.print("Пароль: ");
            String password = scan.nextLine();
            if(password.equals("exit"))return;
            try{
                if(clientDao.isFound(login, password)){
                    currentClient = clientDao.getClient(login);
                    clientMenu();
                    flag = false;
                }
                else{
                    System.out.println("Неверный ввод логина или пароля");
                }
            }catch(SQLException | NullPointerException e){
                System.out.println("Exception: " + e);
                e.printStackTrace();
            }
            
        }
    }
    
    private void registerClient(){
        boolean flag = true;
        while(flag){
            System.out.println("Заполните следующие поля(Введите 'exit' для возврата в меню)");
            System.out.print("Логин: ");
            String login = scan.nextLine();
            if(login.equals("exit"))return;
            try{
                if(! clientDao.isFound(login)){
                System.out.print("ФИО или название организации: ");
                String name = scan.nextLine();
                if(name.equals("exit"))return;
                System.out.print("Пароль: ");
                String password = scan.nextLine();
                if(password.equals("exit"))return;
                clientDao.addClient(new Client(name, login, password));
                System.out.println("Клиент успешно зарегистрирован");
                flag = false;
                }
                else{
                    System.out.println("Клиент с таким логином уже существует. Введите другой логин.");
                }
            }catch(SQLException e){
                System.out.println("Exception: " + e);
                e.printStackTrace();
            }
        }
    }
    
    private void clientMenu(){
        boolean flag = true;
        try{
            if(requestDao.countEmployersRequests() > 4){
                if(requestDao.countWorkersRequests() > 4){
                    distributionOfRequests();
                }
            }
            if(clientsSet != null){
                if(clientsSet.contains(currentClient)){
                    for(Client client : clientsSet){
                        if(client.equals(currentClient)){
                            currentClient = client;
                        }                         
                    }
                }
            }
        }catch(SQLException e){
            System.out.println("Exception: " + e);
            e.printStackTrace();
        }
        System.out.println("Приветствуем вас, " + currentClient.getName() + " !!!");
        while(flag){
            System.out.println("1) Зарегистрировать заявку");
            System.out.println("2) Посмотреть предложения на оставленные заявки");
            System.out.println("3) Удалить все оставленные заявки");
            System.out.println("4) Выход");
            System.out.print("Ввод: ");
            String str = scan.nextLine();
            switch(str){
                case "1" :{
                    registerRequest();
                    break;
                }
                case "2" :{
                    currentClient.printRequests();
                    break;
                }
                case "3" :{
                    deleteAllRequests();
                    break;
                }
                
                case "4" :{
                    flag = false;
                    break;
                }
                default: System.out.println("Ошибка ввода");
  
            }
        }
    }
    
    private void registerRequest(){
        boolean flag = true;
        while(flag){
            System.out.println("1) Заявка на поиск специалиста");
            System.out.println("2) Заявка на поиск работодателя");
            System.out.println("3) Выход");
            System.out.print("Ввод: ");
            String str = scan.nextLine();
            switch(str){
                
                case "1" :{
                    try{
                        System.out.println("Для отмены введите 'cancel'");
                        System.out.print("Должность: ");
                        String job = scan.nextLine();
                        if(job.equals("cancel")) break;
                        System.out.print("Предлагаемая заработная плата ($): ");
                        String pay = scan.nextLine();
                        if(pay.equals("cancel")) break;
                        double payment = Double.parseDouble(pay);
                        System.out.print("Недельная занятость (часы): ");
                        String hour = scan.nextLine();
                        if(hour.equals("cancel")) break;
                        double hoursInWeek = Double.parseDouble(hour);
                        System.out.print("Номер телефона работодателя: ");
                        String number = scan.nextLine();
                        if(number.equals("cancel")) break;
                        Request request = new Request(currentClient.getLogin(), job, payment, hoursInWeek, number, 1);
                        System.out.println("Заявка успешно зарегистрирована");
                        requestDao.addRequest(request);
                    }catch(SQLException e){
                        System.out.println("Exception: " + e);
                        e.printStackTrace();
                    }catch(NumberFormatException e){
                        System.out.println("Ошибка ввода данных, заявка не зарегистрирована");
                    }
                    flag = false;
                    break;
                }
                
                case "2":{
                    try{
                        System.out.println("Для отмены введите 'cancel'");
                        System.out.print("Должность: ");
                        String job = scan.nextLine();
                        if(job.equals("cancel")) break;
                        System.out.print("Желаемая заработная плата ($): ");
                        String pay = scan.nextLine();
                        if(pay.equals("cancel")) break;
                        double payment = Double.parseDouble(pay);
                        System.out.print("Недельная занятость (часы): ");
                        String hour = scan.nextLine();
                        if(hour.equals("cancel")) break;
                        double hoursInWeek = Double.parseDouble(hour);
                        System.out.print("Номер телефона специалиста: ");
                        String number = scan.nextLine();
                        if(number.equals("cancel")) break;
                        Request request = new Request(currentClient.getLogin(), job, payment, hoursInWeek, number, 2);
                        System.out.println("Заявка успешно зарегистрирована");
                        requestDao.addRequest(request);
                    }catch(SQLException e){
                        System.out.println("Exception: " + e);
                        e.printStackTrace();
                    }catch(NumberFormatException e){
                        System.out.println("Ошибка ввода данных, заявка не зарегистрирована");
                    }
                    flag = false;
                    break;
                }
                
                case "3":{
                    flag = false;
                    break;
                }
                
                default: System.out.println("Ошибка ввода");
            }
        }
    }
    
    private void deleteAllRequests(){
        try{
        int number = requestDao.deleteAllRequests(currentClient);
        currentClient.deleteAllRequests();
        System.out.println("Все заявки удалены");
        System.out.println("Количество удаленных заявок: " + number);
        }catch(SQLException e){
            System.out.println("Exception: " + e);
            e.printStackTrace();
        }
    }
    
    private void distributionOfRequests(){
        try{
            List<Request> requests = requestDao.getRequestsList();
            employerRequests = new ArrayList<>();
            workerRequests = new ArrayList<>();
            clientsSet = new HashSet<>();
            Iterator <Request> iterator = requests.iterator();
            while(iterator.hasNext()){
                Request request = iterator.next();
                if(request.getType() == 1){
                    employerRequests.add(request);
                }
                else if(request.getType() == 2){
                    workerRequests.add(request);
                }                           
            }
            for(Request employerReq : employerRequests){
                for(Request workerReq : workerRequests){
                    String job1 = employerReq.getJob();
                    String job2 = workerReq.getJob();
                    Double payment1 = employerReq.getPayment();
                    Double payment2 = workerReq.getPayment();
                    if(job1.equalsIgnoreCase(job2) && payment1 >= (payment2 - 50.0)){
                        Client worker = clientDao.getClient(workerReq.getRequesterLogin());
                        Client employer = clientDao.getClient(employerReq.getRequesterLogin());
                        workerReq.setRequesterName(worker.getName());
                        employerReq.setRequesterName(employer.getName());
                        if(clientsSet.contains(worker)){
                            for(Client client : clientsSet){
                                if(client.equals(worker)){
                                    client.addRequest(employerReq);
                                    break;
                                }
                            }
                        }
                        else{
                            worker.addRequest(employerReq);
                            clientsSet.add(worker);
                        }
                        if(clientsSet.contains(employer)){
                            for(Client client : clientsSet){
                                if(client.equals(employer)){
                                    client.addRequest(workerReq);
                                    break;
                                }                                
                            }
                        }
                        else{
                            employer.addRequest(workerReq);
                            clientsSet.add(employer);
                        }
                    }
                }
               
            }
            
        }catch(SQLException e){
            System.out.println("Exception: " + e);
            e.printStackTrace();
        }
         
    }
    
}
